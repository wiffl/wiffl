class ChangeSlugNotNullableLinks < ActiveRecord::Migration[6.0]
  def change
    change_column :links, :slug, :string, :null => false
  end
end
