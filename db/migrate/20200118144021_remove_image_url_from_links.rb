class RemoveImageUrlFromLinks < ActiveRecord::Migration[6.0]
  def change
    remove_column :links, :image_url
  end
end
