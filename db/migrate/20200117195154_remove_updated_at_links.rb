class RemoveUpdatedAtLinks < ActiveRecord::Migration[6.0]
  def change
    remove_column :links, :updated_at
  end
end
