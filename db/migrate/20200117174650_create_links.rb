class CreateLinks < ActiveRecord::Migration[6.0]
  def change
    create_table :links do |t|
      t.string :slug
      t.text :url
      t.string :title
      t.text :description
      t.string :image_url

      t.timestamps

      t.index :slug, unique: true
    end
  end
end
