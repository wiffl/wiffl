class AddClicksCountToLinks < ActiveRecord::Migration[6.0]
  def change
    add_column :links, :clicks_count, :bigint, :default => 0
  end
end
