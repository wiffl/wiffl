class CreateLinkClicks < ActiveRecord::Migration[6.0]
  def change
    create_table :link_clicks do |t|
      t.bigint :link_id, :null => false
      t.text :user_agent
      t.text :referrer_url
      t.string :ip_address
      t.datetime :created_at, :default => Time.now
    end
  end
end
