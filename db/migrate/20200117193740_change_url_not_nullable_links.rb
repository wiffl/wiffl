class ChangeUrlNotNullableLinks < ActiveRecord::Migration[6.0]
  def change
    change_column :links, :url, :string, :null => false
  end
end
