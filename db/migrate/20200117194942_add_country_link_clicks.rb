class AddCountryLinkClicks < ActiveRecord::Migration[6.0]
  def change
    add_column :link_clicks, :country, :string
  end
end
