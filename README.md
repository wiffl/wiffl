# Wiffl Link Shortener

**Here are a few steps to get the Wiffl link shortener up and running in your local environment:**

1. Make sure you're running a version the following software versions:
- Ruby 2.5.1 or greater ([use RVM](https://rvm.io/) to easily run multiple versions of Ruby))
- Node 8.16.0 or greater (download [NodeJS here](https://nodejs.org/en/))
- Bundler 2 or greater (here is a guide to [update Bundler](https://bundler.io/guides/bundler_2_upgrade.html))
2. Clone the Wiffl repo to a directory on your machine `git clone git@gitlab.com:wiffl/wiffl.git`
3. CD into the Wiffl root directory and run the following commands to get the project set up:
```
bundle install
yarn
```
4. In the root directory of your Wiffl project, change the name of the `.example.env` file to `.env`
5. Now, you have the choice of using MySQL or SQLite for your database locally:
- To use MySQL, set the `DB_ADAPTER` environment variable in your `.env` file to `mysql2` and update the variables below it with your local MySQL database information
- To use SQLite, set the `DB_ADAPTER` environment variable in your `.env` file to `sqlite3`
6. Finally, run `rails server` in the root directory of your Wiffl project

Let me know at info@joshanderton.com if you run into any issues!

**Here is an explanation of how the link shortening algorithm works:**

I take it the current count of all links in the database and, using a Base 58 hash, I find out what the minimum possible digits could be based on how many variations are available to that link count.

This is what the equation looks like:
`digits = (Math.log(number_of_links_in_database, 58) + 1).floor`

Then, just to be safe, I am watching for duplicate keys when I do save the short slug. If for some reason a duplicate slug is found more than 3 times, an additional digit is allowed in the generating of the hash.

```
while new_slug.nil?
  new_slug = SecureRandom.base58(digits)

  if Link.find_by(slug: new_slug).present?
    new_slug = nil
    duplicates += 1
  end

  if duplicates == duplicates_limit
    duplicates = 0
    digits += 1
  end
end
```