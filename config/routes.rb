Rails.application.routes.draw do
  resources :links, :only => [:index, :create]
  get '/:slug', to: 'links#show', as: 'link'
  root 'links#home'
end
