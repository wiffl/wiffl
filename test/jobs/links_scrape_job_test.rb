require 'test_helper'

class LinksScrapeJobTest < ActiveJob::TestCase
  test 'that link url is scraped' do
    link = links(:one)
    LinksScrapeJob.perform_now(link)
    assert (
      link.title == 'Baremetrics: Subscription Analytics & Insights for Stripe, Braintree, Recurly and more!' &&
      link.description == 'Baremetrics is Subscription Analytics and Insights: One click and you get hundreds of valuable metrics and business insights!' &&
      link.logo_url == 'https://baremetrics.com/wp-content/themes/baremetrics/dist/apple-touch-icon.png?v=1'
    )
  end
end
