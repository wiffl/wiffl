require 'test_helper'

class LinksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @link = links(:one)
    @click = link_clicks(:one)
  end

  test 'should get index' do
    get links_url
    assert_select '#links_index .link'
  end

  test 'should create link' do
    assert_difference 'Link.count', +1 do
      post links_url, params: { link: { url: @link.url } }
    end
  end

  test 'should redirect to link url' do
    get link_url(@link.slug)
    assert_redirected_to @link.url
  end

  test 'should update clicks count' do
    assert_difference ['LinkClick.count', 'Link.first.clicks_count'], +1 do
      get link_url(@link.slug)
    end
  end

end
