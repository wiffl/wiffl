require 'test_helper'

class LinkTest < ActiveSupport::TestCase
  test 'link should not save without a url' do
    link = Link.new
    assert_not link.save
  end

  test 'link should not save without a valid URL' do
    link = Link.new(url: 'baremetrics')
    assert_not link.save
  end

  test 'link should not save without a slug' do
    link = Link.new(slug: '', url: 'https://baremetrics.com')
    assert_not link.save
  end

  test 'link should not save with duplicate slug' do
    link1 = Link.create(slug: 'testing', url: 'https://baremetrics.com')
    link2 = Link.new(slug: 'testing', url: 'https://baremetrics.com')
    assert_not link2.save
  end

  test 'link should save with a slug and a valid URL' do
    link = Link.new(slug: 'test', url: 'https://baremetrics.com')
    assert link.save
  end

  test 'click should not save without parent link' do
    link_click = LinkClick.new
    assert_not link_click.save
  end

  test 'click should save with parent link' do
    link_click = LinkClick.new(link_id: 1)
    assert link_click.save
  end
end
