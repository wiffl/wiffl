require 'application_system_test_case'

class LinksTest < ApplicationSystemTestCase
  setup do
    @link = links(:one)
  end

  test 'creating a link' do
    visit '/'
    fill_in 'link_url', with: 'https://baremetrics.com'
    click_on 'Shorten'
    assert_selector '#success.show'
  end

  test 'creating a link and copy it' do
    visit '/'
    fill_in 'link_url', with: 'https://baremetrics.com'
    click_on 'Shorten'
    click_on 'Copy'
    assert_text 'COPIED!'
  end

  test 'creating a second link' do
    visit '/'
    fill_in 'link_url', with: 'https://baremetrics.com'
    click_on 'Shorten'
    fill_in 'link_url', with: 'https://www.profitwell.com'
    click_on 'Shorten'
    assert_selector '#success.show'
  end

  test 'error when submitting no link' do
    visit '/'
    fill_in 'link_url', with: ''
    click_on 'Shorten'
    assert_text "Can't be blank"
    assert_text "That's not a real URL"
  end

  test 'error when submitting invalid link' do
    visit '/'
    fill_in 'link_url', with: 'blah'
    click_on 'Shorten'
    assert_text "That's not a real URL"
  end

  test 'navigate to link index and back' do
    visit '/'
    click_on 'Top 100 Links'
    assert_text 'See what people are clicking on the most.'

    visit links_url
    click_on 'Shorten a link!'
    assert_text 'Links that people want to click'
  end

  test 'reset link create form' do
    visit '/'
    fill_in 'link_url', with: 'https://baremetrics.com'
    click_on 'Shorten'
    fill_in 'link_url', with: 'https://www.profitwell.com'
    assert_selector 'body #success.hide', visible: :hidden
  end
end
