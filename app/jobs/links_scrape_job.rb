class LinksScrapeJob < ApplicationJob
  queue_as :default

  def perform(link)
    url = link.url

    agent = Mechanize.new()
    agent.follow_meta_refresh = true
    page = agent.get(url)

    raise "No page found for #{url}" if page.blank?

    description = page.at("head meta[name='description']")
    description = description.to_s.scan(/\b(?<=content=\")[^"]+(?=\")/)[0]

    logo = page.at("head link[rel='apple-touch-icon']")
    logo = page.at("head link[rel='icon']") if !logo.present?
    logo = logo.to_s.scan(/\b(?<=href=\")[^"]+(?=\")/)[0]

    raise "No data found on page for #{url}" if page.title.blank? && description.blank?

    link.update(
      :title => page.title,
      :description => description.presence,
      :logo_url => logo.presence,
    )
  end
end
