// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")
require("css/application.scss")

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)

document.addEventListener('turbolinks:load', function() {

  var ClipboardJS      = require('clipboard'),
      clipboard        = new ClipboardJS('#copy_link_button'),
      new_link_button  = document.getElementById('new_link_button'),
      new_link_form    = document.getElementById('form_links_create'),
      new_link_field   = document.getElementById('link_url'),
      copy_link_button = document.getElementById('copy_link_button'),
      copy_link_field  = document.getElementById('copy_link_field'),
      success_element  = document.getElementById('success')

  if (new_link_form) {
    new_link_form.addEventListener('submit', function(){
      if (this.className.indexOf('submitting') >= 0) {
        return false;
      }
      new_link_button.innerHTML = new_link_button.dataset.submittingMsg
      this.className+= ' submitting'
    })

    new_link_form.addEventListener('ajax:success', function(event) {
      [data, status, xhr] = event.detail
      this.className = this.className.replace(' submitting', '')
      new_link_button.innerHTML = new_link_button.dataset.initialMsg
      new_link_field.value = ''
      new_link_field.blur()
      copy_link_field.value = data.link_shortened
      success_element.className = success_element.className.replace(' hide', ' show')
    })

    new_link_form.addEventListener('ajax:error', function(event) {
      [data, status, xhr] = event.detail
      console.log(data.errors, status, xhr)

      var error_element = this.querySelector('#error_explanation'),
          message = this.dataset.errorMsg

      this.className = this.className.replace(' submitting', '')
      new_link_button.innerHTML = new_link_button.dataset.initialMsg
      new_link_field.blur()

      if (typeof data.errors === 'object' && Object.keys(data.errors).length > 0) {
        message = ''
        for (var key in data.errors){
          data.errors[key].forEach(function(error){
            message+= `${error.charAt(0).toUpperCase() + error.slice(1)}</br>`
          })
        }
      }

      error_element.innerHTML = message
      error_element.style.opacity = 1

      setTimeout(function(){
        error_element.style.opacity = 0
      }, 4000)
    })
  }

  clipboard.on('success', function(e) {
    e.trigger.innerHTML = e.trigger.dataset.successMsg
    setTimeout(function(){
      e.trigger.innerHTML = e.trigger.dataset.initialMsg
    }, 2000)

    e.clearSelection();
  });

  clipboard.on('error', function(e) {
    console.error('Action:', e.action);
    console.error('Trigger:', e.trigger);
  });

  copy_link_field.addEventListener('click', function(){
    copy_link_button.click();
  })

  new_link_field.addEventListener('focus', function(){
    var success_element = document.getElementById('success')
    success_element.className = success_element.className.replace(' show', ' hide')
    copy_link_field.value = ''
  })

})
