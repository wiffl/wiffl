class LinkClick < ApplicationRecord
  belongs_to :link, dependent: :delete
end
