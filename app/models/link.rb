class Link < ApplicationRecord
  has_many :clicks, class_name: "LinkClick"
  validates :slug, presence:true, uniqueness: true
  validates :url, presence: true, url: { public_suffix: true, message: "That's not a real URL" }

  after_initialize :generate_slug
  after_initialize :clean_url

  scope :top_100, -> {
    order('clicks_count DESC').limit(100)
  }

  def generate_slug
    self.slug = ApplicationController.helpers.generate_unique_short_slug(self.class, :slug) if self.slug.nil?
  end

  def clean_url
    self.url = "https://#{self.url}" if self.url.present? && !self.url.include?('//')
  end
end
