module ApplicationHelper
  def generate_unique_short_slug(model, field)
    slug             = nil
    duplicates       = 0
    duplicates_limit = 3
    records_count    = model.count
    records_count    = 1 if records_count == 0
    digits           = (Math.log(records_count, 58) + 1).floor

    while slug.nil?
      slug = SecureRandom.base58(digits)

      if model.find_by(field => slug).present?
        slug = nil
        duplicates += 1
      end

      if duplicates == duplicates_limit
        duplicates = 0
        digits += 1
      end
    end

    return slug
  end

  def tr(key, variables = {})
    I18n.t key, variables
  end
end
