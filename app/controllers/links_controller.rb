class LinksController < ApplicationController
  before_action :set_link, only: [:show, :edit, :update, :destroy]

  def home
    render '/home'
  end

  def index
    @links = Link.top_100
  end

  def show
    url = @link.url

    @link.clicks.create(
      :user_agent   => request.user_agent,
      :referrer_url => request.referrer,
      :ip_address   => request.remote_ip,
      :country      => request.location.country,
    )

    @link.update(clicks_count: @link.clicks.count)

    redirect_to url
  end

  def create
    @link = Link.new(link_params)

    if @link.save
      LinksScrapeJob.perform_later(@link)
      render json: { notice: 'Yay!', link: @link, link_shortened: link_url(@link.slug) }
    else
      render json: { errors: @link.errors }, status: 404
    end
  end

  private
    def set_link
      @link = Link.find_by(:slug => params[:slug])
    end

    def link_params
      params.require(:link).permit(:url)
    end
end
